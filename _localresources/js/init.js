ccc = {};

ccc.DOMelements = {
	viewport: 				document.getElementsByClassName('viewport'),
	box:					document.getElementsByClassName('box'),
	face:					document.getElementsByClassName('face')
};

ccc.settings = {

	viewport: {
		perspective: {
			types: ["isometric", "perspective"],
			selected: "perspective"
		},
		renderer: {
			types: ["transparent", "solid", "wireframe", "textured"],
			selected: "textured"
		}
	},

	box: {
		camera: {
			types: ["front", "back", "left", "right", "top", "bottom", "angle"],
			selected: "angle"
		}
	},
	
	face: {}
};

ccc.init = function() {

	var viewport = ccc.settings.viewport;
	var perspective = viewport.perspective;
	var renderer = viewport.renderer;
	
	ccc.DOMelements.viewport[0].className = "viewport" + " " +  
		perspective.selected + " " + 
		renderer.selected;

	// viewport -> perspective

	for (var i in perspective.types){
		var val = viewport.perspective.types[i];
		var domElement = document.getElementById(val);
		
		domElement.addEventListener('click', 
			function(){
				
				perspective.selected = this.id;
				
				ccc.DOMelements.viewport[0].className = "viewport" + " " +  
					perspective.selected + " " + 
					renderer.selected;
				
			},
			false
		);
		
	};
	
	// viewport -> renderer

	for (var i in renderer.types){
		var val = viewport.renderer.types[i];
		var domElement = document.getElementById(val);
		
		domElement.addEventListener('click', 
			function(){
				
				renderer.selected = this.id;
				
				ccc.DOMelements.viewport[0].className = "viewport" + " " +  
					perspective.selected + " " + 
					renderer.selected;
				
			},
			false
		);
		
	};






	var box = ccc.settings.box;
	var camera = box.camera;
	
	ccc.DOMelements.box[0].className = "box" + " " +  camera.selected;

	// box -> camera

	for (var i in camera.types){
		var val = box.camera.types[i];
		var domElement = document.getElementById(val);
		
		domElement.addEventListener('click', 
			function(){
				
				camera.selected = this.id;
				ccc.DOMelements.box[0].className = "box" + " " +  camera.selected;				

			},
			false
		);
		
	};

};




window.addEventListener( 'DOMContentLoaded', ccc.init, false);