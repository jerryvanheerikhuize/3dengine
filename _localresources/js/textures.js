var woodThickness = 90;
	
textures = {
	front: 				document.getElementsByClassName('face front'),
	back:				document.getElementsByClassName('face back'),
	top:				document.getElementsByClassName('face top'),
	bottom: 			document.getElementsByClassName('face bottom'),
	left:				document.getElementsByClassName('face left'),
	right:				document.getElementsByClassName('face right')
};


function addLayer(obj,type){
	obj.innerHTML = obj.innerHTML + '<div id="'+type+'" class="'+type+'"></div>';


}

function randomizeFaces (side){
	
	faces = textures[side];
	if (side != 'left' && side != 'right'){
		var tOW = 6803;
		var tOH = 425;
	} else {
		var tOW = 887;
		var tOH = 425;
	}
	
	for (var i in faces){
		// check if real face
		if (!isNaN(i)){
			
			var obj = faces[i];
		
			addLayer (obj,'tint');
			addLayer (obj,'branding');
			addLayer (obj,'lighter');
			
			if (side == 'back'){
				addLayer (obj,'innerShader');
			}
			
			
		
			
			
			var tR = woodThickness / 425;
			var tW = tOW * tR;
			var tH = tOH * tR;
			var tX = Math.floor((Math.random()*tW));
						
						
						
			obj.style.backgroundPosition = tX+"px 0px";
			obj.style.backgroundSize = tW+"px "+tH+"px";
			
			//var lobj = document.getElementById('lighter');
			//lobj.style.backgroundSize = "100% 100%";
			
			// EXTRA RANDOM 50% EFFECT
			if (Math.random() > .5){
				// FLIPS TEXTURE AND CORRECTS POSITION
				//obj.style.webkitTransform = "scale(-1,1)";
				//obj.style.left = obj.offsetWidth+'px';
			}
			
			
			
			
			
		
		}
	}
}

function initRandomization (){
	randomizeFaces('front');
	randomizeFaces('back');
	randomizeFaces('top');
	randomizeFaces('bottom');
	randomizeFaces('left');
	randomizeFaces('right');
}

initRandomization ();